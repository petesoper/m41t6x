/* 
 * ST/Thompson M41T6x (M41T62,65,65) clock/calendar driver
 *
 *  Copyright (c) 2014, 2015, 2017 Peter J. Soper
 *  MIT license (see "LICENSE" in this distribution)
 */

#include <stdint.h>
#include <Arduino.h>
#include <stdio.h>
#include <Wire.h>
#include "M41T6X.h"
#include <avr/pgmspace.h>

const PROGMEM char name0[] PROGMEM = "FRACTIONAL_SECOND ";
const PROGMEM char name1[] PROGMEM = "SECOND            ";
const PROGMEM char name2[] PROGMEM = "MINUTE            ";
const PROGMEM char name3[] PROGMEM = "HOUR              ";
const PROGMEM char name4[] PROGMEM = "DAY_OF_WEEK       ";
const PROGMEM char name5[] PROGMEM = "DAY_OF_MONTH      ";
const PROGMEM char name6[] PROGMEM = "MONTH             ";
const PROGMEM char name7[] PROGMEM = "YEAR              ";
const PROGMEM char name8[] PROGMEM = "CALIBRATION       ";
const PROGMEM char name9[] PROGMEM = "WATCHDOG          ";
const PROGMEM char namea[] PROGMEM = "ALARM_MONTH       ";
const PROGMEM char nameb[] PROGMEM = "ALARM_DAY_OF_MONTH";
const PROGMEM char namec[] PROGMEM = "ALARM_HOUR        ";
const PROGMEM char named[] PROGMEM = "ALARM_MINUTE      ";
const PROGMEM char namee[] PROGMEM = "ALARM_SECOND      ";
const PROGMEM char namef[] PROGMEM = "FLAGS             ";

/*
 * Days per month - (January = 0)
 */

static  unsigned long monthDays[]={31,28,31,30,31,30,31,31,30,31,30,31}; 

/*
 * A formated console (serial) print routine that provides a bit of merciful
 * relief from the Serial.print functions. Straight out of the AVR stdio.h
 * library. If varargs isn't available then to port this for each place 
 * cPrint is used you could substitute something like this:
 * {
 *   char line[73];
 *   snprintf(line,sizeof(line),<args to cPrint>);
 *   Serial.print(line0);
 * }
 */

void M41T6X::cPrint(const char *format, ...) {
  char line[73];
  va_list args;
  va_start(args, format);
  vsnprintf(line, sizeof(line), format, args);
  va_end(args);

  Serial.print(line);
}

void M41T6X::cPrintP(const __FlashStringHelper *fstr, ...) {
  const char * __attribute__((progmem)) p = (const char * ) fstr;
  char line[73];
  char format[72];
  va_list args;
  va_start(args, fstr);
  int i = 0;
  while ((i < 39) && (format[i++] = pgm_read_byte(p++)) != 0) 
    ;
  format[i] = 0;
/*
  Serial.print(" i: ");
  Serial.print(i);
  Serial.print(" ");
  Serial.println(format);
*/
  vsnprintf(line, sizeof(line), format, args);
  va_end(args);

  Serial.print(line);
}

/*
 * Read one chip register
 */

uint8_t M41T6X::readReg(uint8_t offset) {
  uint8_t ret;
  Wire.beginTransmission(M41T6X_ADDR);
  Wire.write(offset); // Start of date/time info
  Wire.endTransmission();
  Wire.requestFrom(M41T6X_ADDR,1);
  ret = Wire.read();
  Wire.endTransmission();
  /* cPrintP(F("Read: %x %s %x\n"), offset, reg_name[offset], ret); */
  return ret;
}

/*
 * Write one chip register
 */

void M41T6X::writeReg(uint8_t offset, uint8_t value) {
  /* cPrintP(F("Write: %x %s %x\n"), offset, reg_name[offset], value); */
  Wire.beginTransmission(M41T6X_ADDR);
  Wire.write(offset);
  Wire.write(value);
  Wire.endTransmission();
}

/*
 * ***** End of local helper functions *****
 */

uint8_t M41T6X::getByte(uint8_t offset) {
  t[offset] = readReg(offset);
  return t[offset];
}

void M41T6X::setByte(uint8_t offset, uint8_t value) {
  writeReg(offset, value);
}

/*
 * Update register from specified byte of local copy t
 */

void M41T6X::updateReg(uint8_t offset) {
  writeReg(offset, t[offset]);
}

void M41T6X::resetWDF() {
  SET_AFIELD(M41T6X_RB01, t, 2);
  SET_AFIELD(M41T6X_BMB, t, 1);
  updateReg(M41T6X_BMB_INDEX);
}

/*
 * Set all chip registers to zero
 */

void M41T6X::zero() {
  for (unsigned int i = 0; i < M41T6X_MAP_SIZE; i++) {
    t[i] = 0;
  }
  updateAllRegs(t);
}

/*
 * TODO: This needs a lot of work
 * 
 * Fully reset the chip, either unconditionally or only if an oscillator fail
 * condition is detected. A reset is ordinarily done when an oscillator stop
 * condition is detected, which is ordinarly after power up. If a battery or
 * supercap are running the chip and it has been initialized previously (or
 * the oscillator started OK without the need for use of the "stop" function)
 * then this reset is not necessary except to set the square wave frequency.
 *
 * NOTICE that this resets the calibration bits to zero!
 */

/*
 * Restart the oscillator
 */

void M41T6X::restart() {
  setST(1);
  delayMicroseconds(10);
  setST(0);
  // Give the oscillator a chance to start */
  delay(1200);
  setOF(0);
}

void M41T6X::reset(uint8_t squareWaveClock, uint8_t unconditional) {
  zero();
  restart();
  setRS(squareWaveClock);
  setSQWE(1);
  /* setOFIE(1); */
  SET_AFIELD(M41T6X_RB01, t, 2);
  SET_AFIELD(M41T6X_BMB, t, 1);
  updateReg(M41T6X_BMB_INDEX);
}

/*
 * Update specified chip regs from local array t
 */

void M41T6X::update(uint8_t *t, uint8_t start, uint8_t limit) {
  for (uint8_t i=start;i<limit;i++) {
    writeReg(i, t[i]);
  }
}

/*
 * Update all chip regs from local array t
 */

void M41T6X::updateAllRegs(uint8_t *t) {
  update(t, 0, M41T6X_MAP_SIZE);
}

/*
 * Update the chip config regs from local array t
 */

void M41T6X::updateConfig(uint8_t *t) {
  update(t, M41T6X_CALIBRATION_INDEX, M41T6X_MAP_SIZE);
}

/*
 * Update the chip date/time regs from local array t
 */

void M41T6X::updateDateTime(uint8_t *t) {
  update(t, 0, M41T6X_CALIBRATION_INDEX);
}

/*
 * Update the chip alarm date/time regs from local array t
 */

void M41T6X::updateAlarmDateTime(uint8_t *t) {
  update(t, M41T6X_ALARM_MONTH_INDEX, M41T6X_WDF_INDEX);
}

/*
 * Set square wave clock speed
 * 0 or 1 - 32khz
 * 2-14 - powers of 2 slower
 * 15 - 1 hz
 */

void M41T6X::setSquareWaveFrequency(int squareWaveClockSpeed) {
  t[M41T6X_RS_INDEX] = readReg(M41T6X_RS_INDEX);
  SET_AFIELD(M41T6X_RS, t, squareWaveClockSpeed);
  updateReg(M41T6X_RS_INDEX);
  t[M41T6X_SQWE_INDEX] = readReg(M41T6X_SQWE_INDEX);
  SET_AFIELD(M41T6X_SQWE, t, 1);
  updateReg(M41T6X_SQWE_INDEX);
}


/*
 * Copy all chip registers into array t
 */

void M41T6X::readAllRegs(uint8_t *t) {
  int i;
  for (i=0;i<=M41T6X_MAP_SIZE;i++) {
    t[i] = readReg(i);
  }
}

/*
 * Return address of local reg copy array t
 */

uint8_t *M41T6X::getT() {
  return t;
}

/*
 * Return individual control/status field values
 *
 * All of these accessors update the given field of the corresponding byte(s) 
 * in local register copy t as a side effect.
 */

uint8_t M41T6X::getST() {
  t[M41T6X_ST_INDEX] = readReg(M41T6X_ST_INDEX);
  return GET_AFIELD(M41T6X_ST,t);
}

uint8_t M41T6X::getOFIE() {
  t[M41T6X_OFIE_INDEX] = readReg(M41T6X_OFIE_INDEX);
  return GET_AFIELD(M41T6X_OFIE,t);
}

uint8_t M41T6X::getRS() {
  t[M41T6X_RS_INDEX] = readReg(M41T6X_RS_INDEX);
  return GET_AFIELD(M41T6X_RS,t);
}

int8_t M41T6X::getCALIBRATION() {
  t[M41T6X_CALIBRATION_INDEX] = readReg(M41T6X_CALIBRATION_INDEX);
  return GET_AFIELD(M41T6X_CALIBRATION,t);
}

uint8_t M41T6X::getFT() {
  t[M41T6X_FT_INDEX] = readReg(M41T6X_FT_INDEX);
  return GET_AFIELD(M41T6X_FT,t);
}

uint8_t M41T6X::getOUT() {
  t[M41T6X_OUT_INDEX] = readReg(M41T6X_OUT_INDEX);
  return GET_AFIELD(M41T6X_OUT,t);
}

uint8_t M41T6X::getRB() {
  t[M41T6X_RB01_INDEX] = readReg(M41T6X_RB01_INDEX);
  return GET_AFIELD(M41T6X_RB01,t) | (GET_AFIELD(M41T6X_RB2, t) << 2);
}

uint8_t M41T6X::getBMB() {
  t[M41T6X_BMB_INDEX] = readReg(M41T6X_BMB_INDEX);
  return GET_AFIELD(M41T6X_BMB,t);
}

uint8_t M41T6X::getC32KE() {
  t[M41T6X_C32KE_INDEX] = readReg(M41T6X_C32KE_INDEX);
  return GET_AFIELD(M41T6X_C32KE,t);
}
uint8_t M41T6X::getSQWE() {
  t[M41T6X_SQWE_INDEX] = readReg(M41T6X_SQWE_INDEX);
  return GET_AFIELD(M41T6X_SQWE,t);
}

uint8_t M41T6X::getAFE() {
  t[M41T6X_AFE_INDEX] = readReg(M41T6X_AFE_INDEX);
  return GET_AFIELD(M41T6X_AFE,t);
}

/*
 * This is nasty because bit field access from scalars haven't been
 * implemented yet.
 *
 * Notice the low order RPT bit is ONE, not ZERO, in contrast to the RB bit
 * numbering.
 */

uint8_t M41T6X::getRPT() {
  t[M41T6X_RPT45_INDEX] = readReg(M41T6X_RPT45_INDEX);
  t[M41T6X_RPT3_INDEX] = readReg(M41T6X_RPT3_INDEX);
  t[M41T6X_RPT2_INDEX] = readReg(M41T6X_RPT2_INDEX);
  t[M41T6X_RPT1_INDEX] = readReg(M41T6X_RPT1_INDEX);
  return GET_AFIELD(M41T6X_RPT1,t) | (GET_AFIELD(M41T6X_RPT2, t) << 1) | (GET_AFIELD(M41T6X_RPT3, t) << 2) | (GET_AFIELD(M41T6X_RPT45, t) << 3);
}

uint8_t M41T6X::getOF() {
  t[M41T6X_OF_INDEX] = readReg(M41T6X_OF_INDEX);
  return GET_AFIELD(M41T6X_OF,t);
}

uint8_t M41T6X::getAF() {
  t[M41T6X_AF_INDEX] = readReg(M41T6X_AF_INDEX);
  return GET_AFIELD(M41T6X_AF,t);
}

uint8_t M41T6X::getWDF() {
  t[M41T6X_WDF_INDEX] = readReg(M41T6X_WDF_INDEX);
  return GET_AFIELD(M41T6X_WDF,t);
}

/*
 * Set individual control/status fields to the given value from 0 to N.
 *
 * All of these mutators update the given field(s) of the corresponding byte(s) 
 * in local register copy array t before the array element(s) are written to
 * the chip. The relevant bytes are READ from the chip, the provided values
 * are put in place in the local register copy, then the bytes are WRITTEN back
 * to the chip.
 */

void M41T6X::setST(uint8_t value) {
  t[M41T6X_ST_INDEX] = readReg(M41T6X_ST_INDEX);
  SET_AFIELD(M41T6X_ST, t, value);
  writeReg(M41T6X_ST_INDEX, t[M41T6X_ST_INDEX]);
}

void M41T6X::setOFIE(uint8_t value) {
  t[M41T6X_OFIE_INDEX] = readReg(M41T6X_OFIE_INDEX);
  SET_AFIELD(M41T6X_OFIE, t, value);
  writeReg(M41T6X_OFIE_INDEX, t[M41T6X_OFIE_INDEX]);
}

void M41T6X::setRS(uint8_t value) {
  t[M41T6X_RS_INDEX] = readReg(M41T6X_RS_INDEX);
  SET_AFIELD(M41T6X_RS, t, value);
  writeReg(M41T6X_RS_INDEX, t[M41T6X_RS_INDEX]);
}

void M41T6X::setCALIBRATION(int8_t value) {
  t[M41T6X_CALIBRATION_INDEX] = readReg(M41T6X_CALIBRATION_INDEX);
  SET_AFIELD(M41T6X_CALIBRATION, t, value);
  writeReg(M41T6X_CALIBRATION_INDEX, t[M41T6X_CALIBRATION_INDEX]);
}

void M41T6X::setFT(uint8_t value) {
  t[M41T6X_FT_INDEX] = readReg(M41T6X_FT_INDEX);
  SET_AFIELD(M41T6X_FT, t, value);
  writeReg(M41T6X_FT_INDEX, t[M41T6X_FT_INDEX]);
}

void M41T6X::setOUT(uint8_t value) {
  t[M41T6X_OUT_INDEX] = readReg(M41T6X_OUT_INDEX);
  SET_AFIELD(M41T6X_OUT, t, value);
  writeReg(M41T6X_OUT_INDEX, t[M41T6X_OUT_INDEX]);
}

void M41T6X::setRB(uint8_t value) {
  t[M41T6X_RB01_INDEX] = readReg(M41T6X_RB01_INDEX);
  SET_AFIELD(M41T6X_RB01, t, (value << M41T6X_RB01_SHIFT) & M41T6X_RB01_MASK);
  SET_AFIELD(M41T6X_RB2, t, (value << M41T6X_RB2_SHIFT) & M41T6X_RB2_MASK);
  writeReg(M41T6X_RB01_INDEX, t[M41T6X_RB01_INDEX]);
}

void M41T6X::setBMB(uint8_t value) {
  t[M41T6X_BMB_INDEX] = readReg(M41T6X_BMB_INDEX);
  SET_AFIELD(M41T6X_BMB, t, value);
  writeReg(M41T6X_BMB_INDEX, t[M41T6X_BMB_INDEX]);
}

void M41T6X::setC32KE(uint8_t value) {
  t[M41T6X_C32KE_INDEX] = readReg(M41T6X_C32KE_INDEX);
  SET_AFIELD(M41T6X_C32KE, t, value);
  writeReg(M41T6X_C32KE_INDEX, t[M41T6X_C32KE_INDEX]);
}

void M41T6X::setSQWE(uint8_t value) {
  t[M41T6X_SQWE_INDEX] = readReg(M41T6X_SQWE_INDEX);
  SET_AFIELD(M41T6X_SQWE, t, value);
  writeReg(M41T6X_SQWE_INDEX, t[M41T6X_SQWE_INDEX]);
}

void M41T6X::setAFE(uint8_t value) {
  t[M41T6X_AFE_INDEX] = readReg(M41T6X_AFE_INDEX);
  SET_AFIELD(M41T6X_AFE, t, value);
  writeReg(M41T6X_AFE_INDEX, t[M41T6X_AFE_INDEX]);
}

void M41T6X::setRPT(uint8_t value) {
  t[M41T6X_RPT45_INDEX] = readReg(M41T6X_RPT45_INDEX);
  t[M41T6X_RPT3_INDEX] = readReg(M41T6X_RPT3_INDEX);
  t[M41T6X_RPT2_INDEX] = readReg(M41T6X_RPT2_INDEX);
  t[M41T6X_RPT1_INDEX] = readReg(M41T6X_RPT1_INDEX);
  SET_AFIELD(M41T6X_RPT45, t, (value << M41T6X_RPT45_SHIFT) & M41T6X_RPT45_MASK);
  SET_AFIELD(M41T6X_RPT3, t, (value << M41T6X_RPT3_SHIFT) & M41T6X_RPT3_MASK);
  SET_AFIELD(M41T6X_RPT2, t, (value << M41T6X_RPT2_SHIFT) & M41T6X_RPT2_MASK);
  SET_AFIELD(M41T6X_RPT1, t, (value << M41T6X_RPT1_SHIFT) & M41T6X_RPT1_MASK);
  writeReg(M41T6X_RPT45_INDEX, t[M41T6X_RPT45_INDEX]);
  writeReg(M41T6X_RPT3_INDEX, t[M41T6X_RPT3_INDEX]);
  writeReg(M41T6X_RPT2_INDEX, t[M41T6X_RPT2_INDEX]);
  writeReg(M41T6X_RPT1_INDEX, t[M41T6X_RPT1_INDEX]);
}

void M41T6X::setOF(uint8_t value) {
  t[M41T6X_OF_INDEX] = readReg(M41T6X_OF_INDEX);
  SET_AFIELD(M41T6X_OF, t, value);
  writeReg(M41T6X_OF_INDEX, t[M41T6X_OF_INDEX]);
}

/*
 * Adjust clock calibration by a positive or negative value.
 * Return zero if the new calibration value would be valid, nonzero otherwise.
 */

uint8_t M41T6X::bumpCalibration(int8_t increment) {
  int8_t cal = getCALIBRATION() + increment; 
  if (! ((cal > 31) || (cal < -32)) ) {
    setCALIBRATION(cal);
    return 0;
  } else {
    return 1;
  }
}
 
/*
 * Encode the date/time registers from a time_t "seconds since 1970" value.
 * Update the current date and time with this information.
 */

#ifdef DEBUG
void p(long vt, long v, const char *msg) {
  Serial.print(vt);
  Serial.print(v);
  Serial.print(F(" - "));
  Serial.print(msg);
  Serial.write(0xa);
}
#endif

/*
 * Convert date and time to Unix reckoning (seconds since Jan 1, 1970)
 */

time_t M41T6X::dateTimeToUnixTime(uint8_t year, uint8_t month, 
		uint8_t day_of_month, uint8_t hour, uint8_t minute, 
		uint8_t second) {
  time_t time = second + (minute * 60) + (hour * 3600) +
	((day_of_month - 1) * 86400);
  int base_year = year - 1970;
  for (int y = 0; y < base_year; y++) {
    for (int m = 0; m < 12; m++) {
      int days = monthDays[m];
      if ((m == 1) && LEAP_YEAR(y)) {
        days = 29;
      }
      time += (days * 86400);
    }
  }
  for (int m = 0; m < month; m++) {
    int days = monthDays[m];
    if ((m == 1) && LEAP_YEAR(year-1970)) {
      days = 29;
    }
    time += (days * 86400);
  }
  return time;
}

void M41T6X::encodeDateTime(time_t timeInput, uint8_t *t){
  uint8_t tmp[M41T6X_MAP_SIZE];

// break the given time_t into time components
// this is a more compact version of the C library localtime function
// note that year is offset from 1970 !!!

  unsigned long year;
  unsigned long month, monthLength;
  unsigned long time;
  unsigned long days;

  time = (uint32_t)timeInput;
  SET_AFIELD(M41T6X_SECOND, tmp,  time % 10);
  SET_AFIELD(M41T6X_TEN_SECOND, tmp, time % 60 / 10);
  time /= 60;
  SET_AFIELD(M41T6X_MINUTE, tmp, time % 10);
  SET_AFIELD(M41T6X_TEN_MINUTE, tmp, time % 60 / 10);
  time /= 60;
  time = time - 4L;
  SET_AFIELD(M41T6X_HOUR, tmp, (time % 24L) % 10);
  SET_AFIELD(M41T6X_TEN_HOUR, tmp, (time % 24L) / 10L);
  time = time + 4L;
  time /= 24;
  SET_AFIELD(M41T6X_DAY_OF_WEEK, tmp, ((time + 4) % 7) + 1);  // Sunday is day 1 

  year = 0;  
  days = 0;
  while((unsigned long)(days += (LEAP_YEAR(year) ? 366 : 365)) <= time) {
    year++;
  }

  SET_AFIELD(M41T6X_YEAR, tmp, (year + 1970) % 10);
  SET_AFIELD(M41T6X_TEN_YEAR, tmp, ((year + 1970) % 100) / 10);

  SET_AFIELD(M41T6X_CENTURY, tmp, (year + 1970) / 100);
  
  days -= LEAP_YEAR(year) ? 366 : 365;
  time  -= days; // now it is days in this year, starting at 0

  days=0;
  month=0;
  monthLength=0;

  for (month=0; month<12; month++) {
    if (month==1) { // february
      if (LEAP_YEAR(year)) {
        monthLength=29;
      } else {
        monthLength=28;
      }
    } else {
      monthLength = monthDays[month];
    }
    
    if (time >= monthLength) {
      time -= monthLength;
    } else {
        break;
    }
  }

  SET_AFIELD(M41T6X_MONTH, tmp, (month + 1) % 10);  // jan is month 1  
  SET_AFIELD(M41T6X_TEN_MONTH, tmp, (month + 1) / 10);  // jan is month 1  

  SET_AFIELD(M41T6X_DAY_OF_MONTH, tmp,(time + 1) % 10);     // day of month 1 origin
  SET_AFIELD(M41T6X_TEN_DAY_OF_MONTH, tmp, (time + 1) / 10);
  SET_AFIELD(M41T6X_HUNDREDTH_SECOND, tmp, 0);
  SET_AFIELD(M41T6X_TENTH_SECOND, tmp, 0);
  updateDateTime(tmp);
}

void M41T6X::setDateTime(time_t timeInput){
  encodeDateTime(timeInput, t);
}

/*
 * Set the alarm date/time and repetition
 */

void M41T6X::setAlarmDateTime(uint8_t month, uint8_t day_of_month, 
	uint8_t hour, uint8_t minute, uint8_t second, uint8_t repeat) {
  readAllRegs(t);
  SET_AFIELD(M41T6X_RPT1, t, repeat & 1);
  SET_AFIELD(M41T6X_RPT2, t, repeat & 2 >> 1);
  SET_AFIELD(M41T6X_RPT3, t, repeat & 4 >> 2);
  SET_AFIELD(M41T6X_RPT45, t, repeat & 0x18 >> 3);
  SET_AFIELD(M41T6X_ALARM_TEN_HOUR, t, hour / 10);
  SET_AFIELD(M41T6X_ALARM_HOUR, t, hour % 10);
  SET_AFIELD(M41T6X_ALARM_TEN_DAY_OF_MONTH, t, day_of_month / 10);
  SET_AFIELD(M41T6X_ALARM_DAY_OF_MONTH, t, day_of_month % 10);
  SET_AFIELD(M41T6X_ALARM_TEN_MINUTE, t, minute / 10);
  SET_AFIELD(M41T6X_ALARM_MINUTE, t, minute % 10);
  SET_AFIELD(M41T6X_ALARM_TEN_MONTH, t, month / 10);
  SET_AFIELD(M41T6X_ALARM_MONTH, t, month % 10);
  SET_AFIELD(M41T6X_AFE, t, 1);
  updateAlarmDateTime(t);
}

/*
 * Set the current date/time. The year is the "real" year, e.g. 2015.
 */

void M41T6X::setDateTime(uint16_t year, uint8_t month, uint8_t day_of_month, 
	uint8_t day_of_week, uint8_t hour, uint8_t minute, uint8_t second) { 
  readAllRegs(t);
  SET_AFIELD(M41T6X_DAY_OF_MONTH, t, day_of_month % 10);
  SET_AFIELD(M41T6X_TEN_DAY_OF_MONTH, t, day_of_month / 10);
  SET_AFIELD(M41T6X_DAY_OF_WEEK, t, day_of_week);
  SET_AFIELD(M41T6X_MONTH, t, month % 10);
  SET_AFIELD(M41T6X_TEN_MONTH, t, month / 10);
  SET_AFIELD(M41T6X_YEAR, t, (year - 2000) % 10);
  SET_AFIELD(M41T6X_TEN_YEAR, t, (year- 2000) / 10);
  SET_AFIELD(M41T6X_CENTURY, t, year / 100);
  SET_AFIELD(M41T6X_HOUR, t, hour % 10);
  SET_AFIELD(M41T6X_TEN_HOUR, t, hour / 10);
  SET_AFIELD(M41T6X_MINUTE, t, minute % 10);
  SET_AFIELD(M41T6X_TEN_MINUTE, t, minute / 10);
  SET_AFIELD(M41T6X_SECOND, t, second % 10);
  SET_AFIELD(M41T6X_TEN_SECOND, t, second / 10);
  SET_AFIELD(M41T6X_HUNDREDTH_SECOND, t, 0);
  SET_AFIELD(M41T6X_TENTH_SECOND, t, 0);
  updateDateTime(t);
}

/*
 * Print the given value as a pair of digits with leading zero
 */

void printAsTwo(int v, const char *l = NULL) {
  if (v < 10) {
    Serial.print(F("0")); 
  }
  Serial.print(v); 
  if (l) {
    Serial.print(l);
  }
}

/*
 * Print the current date and time as raw hex (rawDump nonzero) or formatted
 */

void M41T6X::printDateTime(uint8_t *regmap) {
  if (!regmap) {
    regmap = t;
  }
  readAllRegs(regmap);
  printAsTwo(GET_AFIELD(M41T6X_TEN_DAY_OF_MONTH, regmap) * 10 + GET_AFIELD(M41T6X_DAY_OF_MONTH,regmap), "/");
  printAsTwo(GET_AFIELD(M41T6X_TEN_MONTH, regmap) * 10 + GET_AFIELD(M41T6X_MONTH, regmap),"/");
  printAsTwo(GET_AFIELD(M41T6X_TEN_YEAR, regmap) * 10 + GET_AFIELD(M41T6X_YEAR, regmap) + (GET_AFIELD(M41T6X_CENTURY, regmap) * 100) + 2000," ");
  printAsTwo(GET_AFIELD(M41T6X_TEN_HOUR, regmap) * 10 + GET_AFIELD(M41T6X_HOUR,regmap), ":");
  printAsTwo(GET_AFIELD(M41T6X_TEN_MINUTE, regmap) * 10 + GET_AFIELD(M41T6X_MINUTE, regmap),":");
  printAsTwo(GET_AFIELD(M41T6X_TEN_SECOND, regmap) * 10 + GET_AFIELD(M41T6X_SECOND, regmap),".");
  printAsTwo(GET_AFIELD(M41T6X_TENTH_SECOND, regmap) * 10 + GET_AFIELD(M41T6X_HUNDREDTH_SECOND, regmap));
  Serial.write(0xa);
}

/*
 * Print alarm settings
 */

void M41T6X::printAlarmDateTime() {
  uint8_t t[M41T6X_MAP_SIZE];
  readAllRegs(t);
  Serial.print(F("A: "));
  printAsTwo(GET_AFIELD(M41T6X_ALARM_TEN_DAY_OF_MONTH, t) * 10 + GET_AFIELD(M41T6X_ALARM_DAY_OF_MONTH, t),"/");
  printAsTwo(GET_AFIELD(M41T6X_ALARM_TEN_MONTH, t) * 10 + GET_AFIELD(M41T6X_ALARM_MONTH, t),"/");
  Serial.print(F(" "));
  printAsTwo(GET_AFIELD(M41T6X_ALARM_TEN_HOUR, t) * 10 + GET_AFIELD(M41T6X_ALARM_HOUR, t),":");
  printAsTwo(GET_AFIELD(M41T6X_ALARM_TEN_MINUTE, t) * 10 + GET_AFIELD(M41T6X_ALARM_MINUTE, t), ":");
  printAsTwo(GET_AFIELD(M41T6X_ALARM_TEN_SECOND, t) * 10 + GET_AFIELD(M41T6X_ALARM_SECOND,t), ".");
  Serial.write(0xa);
}

/*
 * Print all control/status regs
 */

void M41T6X::printRegs() {
  uint8_t t[M41T6X_MAP_SIZE];
  readAllRegs(t);
  Serial.print(F("ST: "));
  Serial.print(getST());
  Serial.print(F(" OFIE: "));
  Serial.print(getOFIE());
  Serial.print(F(" RS: "));
  Serial.print(getRS());
  Serial.print(F(" CALIBRATION: "));
  Serial.print(getCALIBRATION());
  Serial.print(F(" FT: "));
  Serial.print(getFT());
  Serial.print(F(" OUT: "));
  Serial.print(getOUT());
  Serial.print(F(" RB: "));
  Serial.print(getRB());
  Serial.print(F(" BMB: "));
  Serial.print(getBMB());
  Serial.write(0xa);
  Serial.print(F(" C32KE: "));
  Serial.print(getC32KE());
  Serial.print(F(" SQWE: "));
  Serial.print(getSQWE());
  Serial.print(F(" AFE: "));
  Serial.print(getAFE());
  Serial.print(F(" AFE: "));
  Serial.print(getAFE());
  Serial.print(F(" RPT: "));
  Serial.print(getRPT());
  Serial.print(F(" OF: "));
  Serial.print(getOF());
  Serial.print(F(" AF: "));
  Serial.print(getAF());
  Serial.print(F(" WDF: "));
  Serial.print(getWDF());
  Serial.write(0xa);
}

/* Print register names of registers that don't have proper power-up vals */

void M41T6X::printSanity() {
  int none = 1;
  if (getST() != 0) {
    Serial.print(F("ST "));  
    none = 0;
  }
  if (getOF() != 1) {
    Serial.print(F("OF "));  
    none = 0;
  }
  if (getOFIE() != 0) {
    Serial.print(F("OFIE "));  
    none = 0;
  }
  if (getOUT() != 1) {
    Serial.print(F("OUT "));  
    none = 0;
  }
  if (getAFE() != 0) {
    Serial.print(F("AFE "));  
    none = 0;
  }
  if (getSQWE() != 1) {
    Serial.print(F("SQWE "));  
    none = 0;
  }
  if (getRS() != 1) {
    Serial.print(F("RS "));  
    none = 0;
  }
  if (getWDF() != 0) {
    Serial.print(F("WDF "));  
    none = 0;
  }
  if (none) {
    Serial.print(F("REGS SANE"));
    Serial.write(0xa);
  } else {
    Serial.print(F("REGS INSANE"));
    Serial.write(0xa);
  }
}

void putP(PGM_P str) {
   char c;
   while ((c = pgm_read_byte(str++)) != 0) {
     Serial.print(c);
   }
}
void M41T6X::printHexRegs() {
  readAllRegs(t);
  for (unsigned int i = 0; i < M41T6X_MAP_SIZE; i++) {
    Serial.print(i, HEX);
    Serial.print(F(" "));
    switch(i) {
      case 0: putP(name0);
	break;
      case 1: putP(name1);
	break;
      case 2: putP(name2);
	break;
      case 3: putP(name3);
	break;
      case 4: putP(name4);
	break;
      case 5: putP(name5);
	break;
      case 6: putP(name6);
	break;
      case 7: putP(name7);
	break;
      case 8: putP(name8);
	break;
      case 9: putP(name9);
	break;
      case 0xa: putP(namea);
	break;
      case 0xb: putP(nameb);
	break;
      case 0xc: putP(namec);
	break;
      case 0xd: putP(named);
	break;
      case 0xe: putP(namee);
	break;
      case 0xf: putP(namef);
	break;
    }
    Serial.print(F(" "));
    Serial.print(t[i], HEX);
    Serial.write(0xa);
  }
}

void M41T6X::begin() {
  /*
  while (getOF() == 1) {
    restart();
    cPrintP(F("restart in begin\n"));
    printHexRegs();
  }
  */
}
