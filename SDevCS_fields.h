#ifndef __SDEVCS_FIELDS
#define __SDEVCS_FIELDS

/*
 * Bit field manipulation
 *
 * These are macros (C #define expressions and statements) for manipulating
 * the fields to do with device register maps. More doc needed.
 *
 * CURRENTLY:
 * ONLY UNSIGNED TYPES ARE ALLOWED FOR BITFIELD MASKS AND REGISTER MAPS
 * MUST BE HELD IN ARRAYS OF TYPE uint8_T, PERIOD.
 *
 * THE MACHINE THESE MACROS ARE USED ON MUST SUPPORT ODD BYTE ALIGNED ACCESS
 * OF ANY MULTIBYTE INTEGERS.
 *
 * THESE MACROS WILL BITE YOU IF YOU TRY TO INSERT A VALUE THAT WON'T FIT
 * INTO THE FIELD. THE EXTRA MASK OPERATION TO SILENTLY KILL THE EXCESS BITS
 * IS *NOT* DONE!
 *
 * Copyright (c) 2015, 2017 Peter James Soper
 * MIT License (see "LICENSE" in this repository)
 */

/* 
 * Don't have stdint.h? Here's what you'll have to do:
 * typedef <some type like unsigned char> uint8_t;
 * typedef <some type like unsigned short or int> uint16_t;
 * etc for the types you intend to use for your register map types (i.e. the
 * size of masks and the size of the smallest integer big enough to contain
 * your largest field)
 */


#include <stdio.h>
#include <stdint.h>

/*
 * If for some reason the data type of shift or index const values 
 * should be different the following two defines can be uncommented and
 * set as needed. For example, to express access to register maps larger
 * than 256 bytes the index type would have to be boosted to uint16_t.
 *
 * TODO: ASK YOURSELF: If a const int allocates no storage what's the
 * big deal with just making these uint16_T??
 * ANSWER? A small machine like an Atmega can more efficiently set up an 8
 * bit immediate but would be forced to load a constant as 16 bits to create
 * an array index if the index type is uint16_t. Is this really true? Have
 * to look at generated code to be convinced.
 */

/* #define USER_SHIFT_TYPE uint8_t */
/* #define USER_INDEX_TYPE uint8_t */

/* Bit number names corresponding to datasheet conventions. The double
   underscore is to avoid collisions with user code. */

#define __D0 0
#define __D1 1
#define __D2 2
#define __D3 3
#define __D4 4
#define __D5 5
#define __D6 6
#define __D7 7
#define __D8 8
#define __D9 9
#define __D10 10
#define __D11 11
#define __D12 12
#define __D13 13
#define __D14 14
#define __D15 15
#define __D16 16
#define __D17 17
#define __D18 18
#define __D19 19
#define __D20 20
#define __D21 21
#define __D22 22
#define __D23 23
#define __D24 24
#define __D25 25
#define __D26 26
#define __D27 27
#define __D28 28
#define __D29 29
#define __D30 30
#define __D31 31

/* The default data type of a shift count or user specified */

#ifdef USER_SHIFT_TYPE
#define SHIFT_TYPE USER_SHIFT_TYPE
#else
#define SHIFT_TYPE uint8_t
#endif


/* The default data type of a shift count or user specified */

#ifdef USER_INDEX_TYPE
#define INDEX_TYPE USER_INDEX_TYPE
#else
#define INDEX_TYPE uint8_t
#endif

/* Set bit n and cast to type, where n goes from LSB of zero to MSB of n and
   type is from stdint.h  */

#define BIT(n, type) ( (type) (1<<(n)) )

/* Create a bit mask len bits long */

#define BIT_MASK(len, type) ( BIT(len, type) - 1 )

/* Create a bit mask len bits long starting at bit position n */

#define MK_MASK(name, start, len, type) \
		const name##_TYPE name##_MASK = BIT_MASK(len, type) << (start);

/* Create a shift count */

#define MK_SHIFT(name, count) const SHIFT_TYPE name##_SHIFT = count;

/* Create a register map byte index */

#define MK_INDEX(name, offset) const INDEX_TYPE name##_INDEX = offset;

/* Clear a bit field */

#define BM_CLEAR(name, v) v &= ~name##_MASK

/* Create bitfield defs for a register map entry */

#define MK_DEF(name, offset, start, len, type) typedef type name##_TYPE; MK_INDEX(name, offset) MK_SHIFT(name, start) MK_MASK(name, start, len, type)

/* Get a bitfield value from a byte */

#define GET_FIELD(name, byte) ((byte & name##_MASK) >> name##_SHIFT)

/* Get a bitfield value from an array byte */

#define GET_AFIELD(name, bytearray) ((*((name##_TYPE *) &bytearray[name##_INDEX]) & name##_MASK) >> name##_SHIFT)

/* Set a bitfield in a byte to a value */

#define SET_FIELD(name, byte, value) byte = (byte & ~name##_MASK) | ((name##_TYPE) ((value) << name##_SHIFT));

/* Set a bitfield in an array byte to a value */

#define SET_AFIELD(name, bytearray, value) *((name##_TYPE *) &bytearray[name##_INDEX]) = (*((name##_TYPE *) &bytearray[name##_INDEX]) & ~name##_MASK) | ((name##_TYPE) ((value) << name##_SHIFT));

#endif /* __SDEVCS_FIELDS */
