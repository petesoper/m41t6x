#ifndef __M41T6X
#define __M41T6X

/*
 *  Copyright (c) 2014, 2015, 2017 Peter J. Soper
 *  MIT License (see "LICENSE" in this repository)
 */

/*
 * ST M41T6{2,4,5} driver
 *
 * TODO: Validate the endian assumptions that might be in this code
 *
 * Package pinout (LCC8)
 *  	pin	function
 *	1	SDA 		I2C serial data
 *	2	SQW		square wave output
 *	3	GND		aka VSS
 *	4	NC
 *	5	VCC		1.3 to 4.4 volts (I2C requires 1.3, clock
 *				will operate at 1.0 v)
 *	6	IRQ\/OUT	active low interrupt or output line (opn drain)
 *	7	NC
 *	8	SCL		I2C serial clock
 */

#include "SDevCS_fields.h"
#include <avr/pgmspace.h>
typedef uint32_t time_t;

/*
 * These macro expansions define typdef, register offset, mask, and shift 
 * counts for the chip's register fields. Macros defined in SDevCS_fields.h
 * support read/write field access given a register buffer array of uint8_t.
 * Notice that the calibration field is a signed integer.
 */

MK_DEF(M41T6X_HUNDREDTH_SECOND, 0, __D0, 4, uint8_t)/* 0-9 */
MK_DEF(M41T6X_TENTH_SECOND, 0, __D4, 4, uint8_t)	/* 0-9 */
MK_DEF(M41T6X_SECOND, 1, __D0, 4, uint8_t)	/* 0-9 */
MK_DEF(M41T6X_TEN_SECOND, 1, __D4, 4, uint8_t)	/* 0-5 */
MK_DEF(M41T6X_ST, 1, __D7, 1, uint8_t)		/* 0-1 */
MK_DEF(M41T6X_MINUTE, 2, __D0, 4, uint8_t)	/* 0-9 */
MK_DEF(M41T6X_TEN_MINUTE, 2, __D4, 4, uint8_t)	/* 0-5 */
/* M41T6X62 and 65 only */
MK_DEF(M41T6X_OFIE, 2, __D7, 1, uint8_t)	/* 0-1 */
MK_DEF(M41T6X_HOUR, 3, __D0, 4, uint8_t)	/* 0-9 */
MK_DEF(M41T6X_TEN_HOUR, 3, __D4, 2, uint8_t)	/* 0-2 */
MK_DEF(M41T6X_DAY_OF_WEEK, 4, __D0, 3, uint8_t)	/* 1-7 */
MK_DEF(M41T6X_RS, 4, __D4, 4, uint8_t)		/* 1-7 */
MK_DEF(M41T6X_DAY_OF_MONTH, 5, __D0, 4, uint8_t)/* 0-9 */
MK_DEF(M41T6X_TEN_DAY_OF_MONTH, 5, __D4, 2, uint8_t)/* 0-3 */
MK_DEF(M41T6X_MONTH, 6, __D0, 4, uint8_t)	/* 0-9 */
MK_DEF(M41T6X_TEN_MONTH, 6, __D4, 1, uint8_t)	/* 0-1 */
MK_DEF(M41T6X_CENTURY, 6, __D6, 2, uint8_t)	/* 0-3 */
MK_DEF(M41T6X_YEAR, 7, __D0, 4, uint8_t)	/* 0-9 */
MK_DEF(M41T6X_TEN_YEAR, 7, __D4, 4, uint8_t)	/* 0-9 */
/* THIS FIELD INCLUDES THE "S" FIELD WHICH IS SIMPLY THE SIGN BIT */
MK_DEF(M41T6X_CALIBRATION, 8, __D0, 5, int8_t)	/* 0-31 */
/* M41T6X62 and 65 only */
MK_DEF(M41T6X_FT, 8, __D6, 1, uint8_t)		/* 0-1 */
/* M41T6X62 and 65 only */
MK_DEF(M41T6X_OUT, 8, __D7, 1, uint8_t)		/* 0-1 */
MK_DEF(M41T6X_RB01, 9, __D0, 2, uint8_t)	/* 0-3 combine with RB2 */
						/* see GET_RB macro */
MK_DEF(M41T6X_BMB, 9, __D2, 5, uint8_t)		/* 0-31 */
MK_DEF(M41T6X_RB2, 9, __D7, 1, uint8_t)		/* 0-1 combine with RB01 */
						/* see GET_RB macro */
MK_DEF(M41T6X_ALARM_MONTH, 0xA, __D0, 4, uint8_t)/* 0-9 */
MK_DEF(M41T6X_ALARM_TEN_MONTH, 0xA, __D4, 1, uint8_t)/* 0-1 */
/* M41T6X64 only */
MK_DEF(M41T6X_C32KE, 0xA, __D5, 1, uint8_t)	/* 0-1 */
MK_DEF(M41T6X_SQWE, 0xA, __D6, 1, uint8_t)	/* 0-1 */
/* M41T6X62 and 65 only */
MK_DEF(M41T6X_AFE, 0xA, __D7, 1, uint8_t)	/* 0-1 */
MK_DEF(M41T6X_ALARM_DAY_OF_MONTH, 0xB, __D0, 4, uint8_t)/* 0-9 */
MK_DEF(M41T6X_ALARM_TEN_DAY_OF_MONTH, 0xB, __D4, 2, uint8_t)/* 0-3 */
MK_DEF(M41T6X_RPT45, 0xB, __D6, 2, uint8_t)	/* 0-3 combine with RPT1,2,3 */
						/* see GET_RPT */
MK_DEF(M41T6X_ALARM_HOUR, 0XC, __D0, 4, uint8_t)/* 0-9 */
MK_DEF(M41T6X_ALARM_TEN_HOUR, 0XC, __D4, 2, uint8_t)/* 0-2 */
MK_DEF(M41T6X_RPT3, 0XC, __D7, 1, uint8_t)	/* 0-1 combine with RPT2,3,45 */
						/* see GET_RPT */
MK_DEF(M41T6X_ALARM_MINUTE, 0xD, __D0, 4, uint8_t)/* 0-9 */
MK_DEF(M41T6X_ALARM_TEN_MINUTE, 0xD, __D4, 2, uint8_t)/* 0-5 */
MK_DEF(M41T6X_RPT2, 0xD, __D7, 1, uint8_t)	/* 0-1 combine with RPT1,3,45 */
						/* see GET_RPT */
MK_DEF(M41T6X_ALARM_SECOND, 0xE, __D0, 4, uint8_t)/* 0-9 */
MK_DEF(M41T6X_ALARM_TEN_SECOND, 0xE, __D4, 2, uint8_t)/* 0-5 */
MK_DEF(M41T6X_RPT1, 0xE, __D7, 1, uint8_t)	/* 0-1 combine with RPT2,3,45 */
						/* see GET_RPT */
MK_DEF(M41T6X_OF, 0xF, __D2, 1, uint8_t)	/* 0-1 */
MK_DEF(M41T6X_AF, 0xF, __D6, 1, uint8_t)	/* 0-1 */
MK_DEF(M41T6X_WDF, 0xF, __D7, 1, uint8_t)	/* 0-1 */

#define M41T6X_MAP_SIZE 16			/* bytes in complete map */

#define M41T6X_ADDR	0x68			/* I2C address of this chip */

#define bcd2bin(x) ((((x)>>4)*10) + (x&0xf))
#define LEAP_YEAR_ACTUAL(Y)     ( ((Y)>0) && !((Y)%4) && ( ((Y)%100) || !((Y)%400) ) )
#define LEAP_YEAR(Y)     ( ((1970+Y)>0) && !((1970+Y)%4) && ( ((1970+Y)%100) || !((1970+Y)%400) ) )

#define PRINT
#define ALT_SET

class M41T6X {
  private:
    uint8_t t[M41T6X_MAP_SIZE];
  public:
    void begin();
    uint8_t *getT();   
    void zero();
    void mutate();
    void reset(uint8_t squareWaveClock, uint8_t unconditional = 1);
    void restart();
    void setSquareWaveFrequency(int squareWaveClockSpeed);
    void cPrint(const char *format, ...);
    void cPrintP(const __FlashStringHelper *fstr, ...);
    void printSanity();
    void printRegs();
    void printDateTime(uint8_t *regmap = NULL);
    void printAlarmDateTime();
    void printHexRegs();
    void setAlarmDateTime(uint8_t month, uint8_t day_of_month, 
	uint8_t hour, uint8_t minute, uint8_t second, uint8_t repeat); 
    void setDateTime(uint16_t year, uint8_t month, uint8_t day_of_month, 
	uint8_t day_of_week, uint8_t hour, uint8_t minute, uint8_t second); 
    void setDateTime(time_t t);
    void setAlarmDateTime(time_t t);
    time_t dateTimeToUnixTime(uint8_t year, uint8_t month, 
	uint8_t day_of_month, uint8_t hour, uint8_t minute, uint8_t second); 
    void encodeDateTime(time_t time, uint8_t *t);
    uint8_t bumpCalibration(int8_t value); 
    void resetWDF();
    uint8_t getByte(uint8_t offset);
    uint8_t getST();
    uint8_t getOFIE();
    uint8_t getRS();
    /* NOTICE THIS RETURNS A SIGNED INTEGER */
    int8_t getCALIBRATION();
    uint8_t getFT();
    uint8_t getOUT();
    uint8_t getRB();
    uint8_t getBMB();
    uint8_t getC32KE();
    uint8_t getSQWE();
    uint8_t getAFE();
    uint8_t getRPT();
    uint8_t getOF();
    uint8_t getAF();
    uint8_t getWDF();
    void setByte(uint8_t offset, uint8_t value);
    void setST(uint8_t value);
    void setOFIE(uint8_t value);
    void setRS(uint8_t value);
    /* NOTICE THIS TAKES A SIGNED INTEGER */
    void setCALIBRATION(int8_t value);
    void setFT(uint8_t value);
    void setOUT(uint8_t value);
    void setRB(uint8_t value);
    void setBMB(uint8_t value);
    void setC32KE(uint8_t value);
    void setSQWE(uint8_t value);
    void setAFE(uint8_t value);
    void setRPT(uint8_t value);
    void setOF(uint8_t value);
  private:
    uint8_t readReg(uint8_t offset);
    void writeReg(uint8_t offset, uint8_t value);
    void update(uint8_t *t, uint8_t start = 0, uint8_t limit = M41T6X_MAP_SIZE);
    void updateReg(uint8_t offset);
    void updateAllRegs(uint8_t *t);
    void updateDateTime(uint8_t *t);
    void updateAlarmDateTime(uint8_t *t);
    void updateConfig(uint8_t *t);
    void readAllRegs(uint8_t *t);
}; /* class M41T6X */

#endif /* __M41T6X6X */
