/*
 * Test M41T6X driver
 *
 *  Copyright (c) 2014, 2015, 2017 Peter J. Soper
 *  MIT License (see "LICENSE" in this repository)
 */

#include <Arduino.h>
#include <Wire.h>
#include "M41T6X.h"

M41T6X rtc;

volatile static unsigned long int0, int1, last_int0, last_int1, 
	wdcount0, wdcount1, ofcount0, ofcount1;

void cRead(const char *format, ...) {
  char line[73];
  uint16_t i=-1;
  va_list args;
   
  do {
    i += 1;
    if (i > (sizeof(line) - 1)) {
      rtc.cPrintP(F("\nLINE TOO LONG!\n"));
      break;
    }
    line[i] = Serial.read();
    Serial.write(line[i]);
  } while(line[i] != 0xa);
  line[i] = 0;
  va_start(args, format);
  sscanf(line, format, args);
  va_end(args);
}

void int0Callback() {
  detachInterrupt(0);
  detachInterrupt(1);
  rtc.reset(1,1);
  if (rtc.getOF() == 1) {
    ofcount0 += 1;
    rtc.restart();
  }else if (rtc.getWDF() == 1) {
    wdcount0 += 1;
    rtc.setByte(M41T6X_RB01_INDEX, 0);
    rtc.resetWDF();
  } else {
    int0 += 1;
  }
}

void int1Callback() {
  detachInterrupt(0);
  detachInterrupt(1);
  if (rtc.getOF() == 1) {
    ofcount1 += 1;
    rtc.restart();
  } else if (rtc.getWDF() == 1) {
    wdcount1 += 1;
    rtc.setByte(M41T6X_RB01_INDEX, 0);
    rtc.resetWDF();
  } else {
    int1 += 1;
  }
}

uint8_t keyboardChar() {
  while (Serial.available() == 0)
    ;
  return Serial.read();
}

/*
 * To do: probe the chip and detect:
 *   I2C failure
 *   broken/insane clock chip behavior
 */

void setup() {
  Wire.begin();
  uint8_t tmp[M41T6X_MAP_SIZE];
  delay(100);
  Serial.begin(9600);
  delay(100);
  pinMode(13,OUTPUT);
/*
  pinMode(2, INPUT_PULLUP);
  pinMode(3, INPUT_PULLUP);
*/
  rtc.begin();
  /* Sat Oct 17 15:03:15 EDT 2015 */
  rtc.encodeDateTime(1445108595L, tmp);
}

void printInfo() {
  rtc.printDateTime(rtc.getT());
  rtc.printAlarmDateTime();
  rtc.printRegs();
  rtc.cPrintP(F("Int0: %d Int1: %d wdcount0 %d "), int0, int1, wdcount0);
  rtc.cPrintP(F("wdcount1 %d ofcount0 %d ofcount1 %d\n"), wdcount1, ofcount0, ofcount1);
}

static const char commands[] = "qmrfsdhinwzqo";

void printPrompt() {

#define C_QUIT 0
#define C_MONITOR 1
#define C_RESET 2
#define C_CAL_FASTER 3
#define C_CAL_SLOWER 4
#define C_SET_DATE_TIME 5
#define C_HEX_DUMP 6
#define C_IA 7
#define C_ID 8
#define C_WD_ENABLE 9
#define C_WD_DISABLE 10
#define C_SQW_ENABLE 11
#define C_SQW_DISABLE 12

  rtc.cPrintP(F("d - date and time set\n"));
  rtc.cPrintP(F("f - faster\n"));
  rtc.cPrintP(F("h - hex register dump\n"));
  rtc.cPrintP(F("i - interrupt enable\n"));
  rtc.cPrintP(F("m - monitor\n"));
  rtc.cPrintP(F("n - no interrupts (disable)\n"));
  rtc.cPrintP(F("q - quit\n"));
  rtc.cPrintP(F("r - reset\n"));
  rtc.cPrintP(F("s - slower\n"));
  rtc.cPrintP(F("w - watchdog enable\n"));
  rtc.cPrintP(F("z - zero watchdog\n"));
  rtc.cPrintP(F("q - square wave enable\n"));
  rtc.cPrintP(F("o - square wave disable\n"));
  rtc.cPrintP(F("Enter command:"));
  Serial.flush();
}

static int delayValue = 5000;

void loop() {
  char ch;
  uint8_t command_index = 0;
  printInfo();
  printPrompt();
  Serial.flush();
  do {
    ch = keyboardChar();
  } while (ch <= ' ');
  while (commands[command_index]) {
    if (commands[command_index] == ch) {
        rtc.cPrintP(F("command: %d\n"), command_index);
	break;
    } else {
	command_index += 1;
    }
  }
  if (command_index >= strlen(commands)) {
    rtc.cPrintP(F("Command not found\n"));
    return;
  }
  switch (command_index) {
    case C_QUIT:
	while(1)
	  ;
        break;
    case C_MONITOR: {
          rtc.cPrintP(F("Enter x to stop monitor %d\n"), delayValue);
	  while (1) {
            printInfo();
            Serial.flush();
	    char c = 0;
	    if (Serial.available() != 0) {
	      c = Serial.read();
            }
	    if (c == 'f') {
	      delayValue = delayValue / 2;
	      if (delayValue <= 0) {
		delayValue = 1;
	      }
	      rtc.cPrintP(F("(%d)\n"), delayValue);
              Serial.flush();
	    } else if (c == 's') {
	      delayValue = delayValue * 2;
	      rtc.cPrintP(F("(%d)\n"), delayValue);
              Serial.flush();
	    } else if (c == 'x') {
	      break;
            } 
            rtc.cPrintP(F("Monitor (%d)\n"), delayValue);
	    delay(delayValue);
          }
        }
        break;
    case C_RESET:
        rtc.reset(1, 1);
        break;
    case C_CAL_FASTER:
        rtc.bumpCalibration(1);
        break;
    case C_CAL_SLOWER:
        rtc.bumpCalibration(-1);
        break;
    case C_SET_DATE_TIME:
        /* Sat Oct 17 15:03:15 EDT 2015 */
        rtc.setDateTime(2015, 10, 17, 6, 15, 3, 15);
        break;
    case C_HEX_DUMP:
        rtc.printHexRegs();
        break;
    case C_IA:
        attachInterrupt(0, int0Callback, RISING);
        attachInterrupt(1, int1Callback, RISING);
        break;
    case C_ID:
        detachInterrupt(0);
        detachInterrupt(1);
        break;
    case C_WD_ENABLE:
	rtc.setByte(M41T6X_BMB_INDEX,6);
        break;
    case C_WD_DISABLE:
	rtc.setByte(M41T6X_BMB_INDEX,0);
        break;
    case C_SQW_ENABLE:
        rtc.setSQWE(1);
        break;
    case C_SQW_DISABLE:
        rtc.setSQWE(0);
        break;
    default:
	Serial.println(F("Unknown command %d\n"));
  }
  rtc.cPrintP(F("BOTTOM OF LOOP\n"));
  Serial.flush();
}
